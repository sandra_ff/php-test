<?php

$path = __DIR__ . DIRECTORY_SEPARATOR;
require $path . 'Connector.php';
require $path . 'PokedexList.php';
require $path . 'PokedexItem.php';
require $path . 'PokedexSearch.php';

use vreemtPokedex\Connector;

$apiURL = 'https://pokeapi.co/api/v2/';



$test = new Connector($apiURL);

if (!$test) 
{
    error_log("Something went wrong with the API.");
    $error = "Can't connect to the Pokémon";
    die();
}

$allPokemon = $test->getAllPokemon();
