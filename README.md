# UKFast Programming test 

## ukfast/pokédex

* Programming challenge: create a Pokédex
* Focus on usability, performance, security
* Parties: SK (Sandra Koning/vreemt), HP @ UKFast 
* Timings: two to three hours
* Deliverables: send URL of solution to UKFast

### Notes

This repository was cloned from https://gitlab.com/ukfast/php-test  
and is available at https://gitlab.com/sandra_ff/php-test

For more context see UKFast_README.md

## To access this project

This solution is accessible with all modern browsers.

### As a user

* Visit [URL](http://)
* Browse the list, click on a name to see the details
* Enter a full name in the search box and hit enter to see details for a specific Pokémon

### As a developer 

* Make sure you have recent versions of
	- PHP (minimum 7.1)
	- [Composer](https://getcomposer.org/)
* Check your PATHs and environment are up to date
	* if on windows, check your php settings if CURLOPT_SSL_VERIFYPEER doesn't work
* Create your project
	* Download the source files (or use `git` to clone the repository)
	* Run `composer start` in the project folder
		* Or: start a dev server with e.g. `php -S localhost:8080 -t public`
* Access <http://localhost:8080> in a browser

## Resources used

* [Pokéapi](https://pokeapi.co/)
* [H5BP](https://html5boilerplate.com/)
* [PokeAPI Wrapper](https://github.com/lmerotta/phpokeapi)
* [Postman](https://www.getpostman.com/)
* [W3C validator](https://validator.w3.org/)
* [WCAG checker](https://achecker.ca/checker/index.php)

## Task requirements

Display a full list, display a single Pokémon, implement some form of search.

* For each item, display
	- At least one image of the Pokémon
	- Name
	- Species
	- Height and weight
	- Abilities

## Development environment

* PokeAPI v2; H5BP 7.2.0

* Windows 10 Pro, 64 bit
	* git version 2.22.0.windows.1
	* PHP 7.3.6; Composer 1.8.6; Sublime 3.2.1
	* Firefox 67.0.2; Chrome 75.0.3770.100
	* Postman 4.10.7
* Linux Mint 18.3 Cinnamon, 64 bit
	* git version 2.7.4
	* PHP 7.3.0; Composer 1.8.0; Sublime 3.1.1
	* Firefox 66.0.1; Chromium 73.0.3683.75

## Steps

* Create README with interpretation and notes on project
* Check environment and requirements
	* Composer IS NOT necessary to 'use' the project as-is
* Install packages with composer
	* I needed to `composer require psr/simple-cache` as it wasn't picked up 
* Implement Boilerplate
* Use wrapper to connect with API
* Create functionality and display for list
* Create functionality and display for item
* Create functionality and display for search
* Test, tidy, tweak
* Update code & readme
* Deliver

## Copyright

All trademarks as the property of their respective owners.
