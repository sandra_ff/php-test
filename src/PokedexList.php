<?php

namespace vreemtPokedex;

/**
 * Class PokedexList
 * @package vreemtPokedex
 */
class PokedexList
{

    /**
     * @var string
     */
    private $apiBaseUrl;

    /**
     * @var array
     */
    private $data;

    /**
     * @var boolean
     */
    private $pretty;

    /**
     * List constructor.
     * @param array $data
     */
    public function __construct(string $apiBaseUrl, array $data, boolean $raw = null)
    {
        $this->apiBaseUrl = $apiBaseUrl;
        $this->data = $data;
        $this->raw = $raw?:false;
    }

    /**
     * @return array of pokemon with names 
     */
    public function buildList()
    {
        
        foreach ($this->data as $i => $listItem)
        {
            if (isset($listItem["name"]))
            {
                $listItem["pokedexName"] = $listItem["name"];
                if (!$this->raw)
                {
                    $listItem["pokedexName"] = $this->prettyName($listItem["name"]);
                }
            }
            else
            {
                $listItem["pokedexName"] = $listItem["name"] = "(name not found)";
            }

            if (isset($listItem["url"]))
            {
                $listItem["pokedexUrlParts"] = $this->linkParts($listItem["url"]);
            }

            $this->data[$i] = $listItem;
        }
        return $this->data;
    }

    private function prettyName($name)
    {
        //capitalise first letter
        return ucfirst($name);
    }

    private function linkParts($apiUrl)
    {
        //https://pokeapi.co/api/v2/pokemon/78/    
        //remove api base url 
        $partLink = str_ireplace($this->apiBaseUrl, '', $apiUrl);
        $parts = explode ( '/' , $partLink);

        $linkParts = [ "part" => $partLink ];
        if (isset($parts[0]) && isset($parts[1]))
        {
            $linkParts["type"] = $parts[0];
            $linkParts["id"] = $parts[1];
        }
        
        return $linkParts;
    }

}