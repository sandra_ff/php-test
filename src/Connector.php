<?php

namespace vreemtPokedex;



use vreemtPokedex\PokedexList;
use vreemtPokedex\PokedexDetail;
use vreemtPokedex\PokedexSearch;

/**
 * Class Connector
 * @package vreemtPokedex
 */
class Connector
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $count;

    /**
     * @var int
     */
    private $currentCount;


    /**
     * Connector constructor.
     * @param string $url
     * @param Client $client
     */
    public function __construct(string $url = 'https://pokeapi.co/api/v2/')
    {
        $this->baseUrl = $url;
        $this->limit = 50;
        $this->count = 100; // needs to be positive to start the fetch
    }

    /**
     * @return mixed
     */
    public function getAllPokemon()
    {
        $pokemonList = array();
        $currentCount = $this->currentCount?:0;
        while ($this->count != 0 && ($currentCount < $this->count)) 
        {
            $getPokemon = $this->getPartList($currentCount);
            $pokemonList = array_merge($pokemonList, $getPokemon);

            $currentCount += $this->limit;
            if ($currentCount >= $this->count || !$getPokemon)
            {
                break;
            }
        }

        $newList = new PokedexList($this->baseUrl, $pokemonList);

        return $newList->buildList(); // $pokemonList;
    }

    /**
    * @param int offset specifies where to start
    * @return mixed array of Pokemon or bool on error
    **/
    public function getPartList(int $offset = 0)
    {
        $pags = '?offset='.$offset.'&limit='.$this->limit;
        $url = $this->baseUrl . 'pokemon/'.$pags;
        $data = $this->doCurl($url);

        $list = $data;

        if (isset($data["results"]))
        { 
            $list = $data["results"];
        }

        if (array_key_exists("previous", $data) && !$data["previous"] && isset($data["count"]))
        {
            //if this is the first part, get total count
            $this->count = $data["count"]; //964
        }    

        return $list;
    }

    /** 
    * @param url inc base
    * @return mixed decoded json on success, bool on failure
    **/
    private function doCurl($url)
    {
        $ch = curl_init();
        $timeout = 5;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $data = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code != 200) {
            $message = sprintf("Error occured %s. URL: %s. RESPONSE: %s", $http_code, $url, $data);
            throw new \Exception($message);
        }

        return json_decode($data, true);
    }
}