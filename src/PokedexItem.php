<?php

namespace vreemtPokedex;

/**
 * Class PokedexItem
 * @package vreemtPokedex
 */
class PokedexItem
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $raw;

    /**
     * List constructor.
     * @param array $data
     */
    public function __construct(array $data, boolean $raw = null)
    {
        $this->data = $data;
        $this->raw = $raw?:false;
    }

    /**
     * @return array of pokemon details
     */
    public function buildItem()
    {
        return $data;
    }

}