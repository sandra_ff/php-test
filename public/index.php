<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <title>Pokédex</title>
  <meta name="description" content="Get a list of Pokémon in the main game, search through them, get details on single Pokémon">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="UKFast, H5BP, Sandra Koning">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <!--[if IE]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->


  <h1>Browse some Pokémon</h1>

<?php
/*
* search
* list
* detail
*/

$appPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;

// Load the composer autoloader
//$autoload = require $appPath . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

// Get the stuff to find those creatures!
$start = require $appPath . 'src' . DIRECTORY_SEPARATOR . 'get_pokemon.php';

?>

<?php if (isset($error))
{ ?><h2>Something went wrong</h2>
  <pre>
<?php 
  echo $error;
?>
  </pre><?php   
}
?>

<?php if (isset($allPokemon))
{ ?><h2>All Pokémon</h2>

<ul><?php 
  foreach ($allPokemon as $i => $listItem)
  {    
    //get name
    $name = $listItem["pokedexName"];    

    $parts = $listItem["pokedexUrlParts"];
    //get id    
    $id = $listItem["pokedexUrlParts"]["id"];
    $type = $listItem["pokedexUrlParts"]["type"];

    //build link
    $link = '?'.$type.'='.$id;

?>
    <li><a href="<?php echo $link; ?>"><?php echo $name; ?></a><!-- index <?php echo $i; ?>; api id <?php echo $id; ?> --></li>
<?php   
  } 
?>
</ul>
<?  
}
?>

  <!-- H5BP js at foot of page -->
  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
</body>

</html>
