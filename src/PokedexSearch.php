<?php

namespace vreemtPokedex;

/**
 * Class PokedexSearch
 * @package vreemtPokedex
 */
class PokedexSearch
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $query;

    /**
     * @var boolean
     */
    private $pretty;

    /**
     * List constructor.
     * @param array $data
     */
    public function __construct(string $query, array $data, boolean $raw = null)
    {
        $this->data = $data;
        $this->query = $query;
        $this->raw = $raw?:false;
    }

    /**
     * @return array of pokemon that match search query
     */
    public function doSearch()
    {
        $found = ["query" => $query, "result" => $data];
        return $found;
    }

}